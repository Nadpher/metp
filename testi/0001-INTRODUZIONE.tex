%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode
% !TEX root = ../metp.tex

\chapter*{INTRODUZIONE}
\addcontentsline{toc}{chapter}{INTRODUZIONE}

%\begin{adjustwidth}{23mm}{0mm}

Ho iniziato questo lavoro di scrittura seguendo l'esigenza di organizzare un testo
che stabilisca un equilibrio tra didattica e creatività nel percorso di studio
della Musica Elettronica, nelle sue implicazioni Tecnologico-Musicali e,
nondimeno, nella tradizione che ne ha strutturato il pensiero. Non è un manuale.
Vuole essere una raccolta organizzata di appunti che segua un percorso di
esperienze reali, un percorso attuale, chiaro e progressivo attraverso gli anni
di studio della materia.

L'esigenza di mettere in un contenitore questi materiali viene direttamente
dalle esperienze di insegnamento, in diversi gradi del percorso formativo
elettroacustico. La bibliografia specifica è ricca ed in continua evoluzione
data la materia viva che la alimenta. Nonostante questo, in lingua italiana, i
testi attualmente di riferimento hanno un assetto ludico, d'intrattenimento.
Organizzati ad unità come fossero un corso di lingua, a livelli, trasformano
argomenti musicali in attitudini ricreative, rafforzando l'idea di didattica
applicata, alla base di troppe classi di musica elettronica in Italia.

Un riferimento letterario cardine però, durevole nel tempo, è il testo di
Walter Branchi \emph{Tecnologia della Musica Elettronica} che dal lontano 1975
ancora riesce a suggerire un'idea di didattica organica e progressiva
ineguagliata, con i difetti di un'opera prima in tutti i sensi, ma con la
prospettiva dello studio scolastico organizzato. A questo testo si ispira questa
raccolta, \emph{METP}, all'idea di un percorso di comprensione che viene da una
visione di scuola, senza la pretesa di creare un abile manovratore di pomelli
in ogni possibile lettore. Le problematiche essenziali sono altre, e sono, sempre
prendendo spunto dal lavoro di Branchi, ancora egregiamente espresse dalle
parole di Domenico Guaccero nella prefazione:

\begin{quote}
  \ldots val la pena ricordare e mantenere fermi gli apporti che la “musica
  elettronica” ha introdotto nell'opera musicale\ldots E cioè l'andare alla
  radice del suono, come fatto fisico e (anche) da qui come fatto musicale, la
  possibilità di liberarsi definitivamente dal “dover costruire” sistemi
  intervallari, il porre il problema del rapporto musica esecutore e musica
  spettacolo in termini fino allora inusitati, di riproporre, ma allargandone
  permamentemente i confini, il rapporto tra tecnologia e composizione.
\end{quote}

In questa ottica di indagine, alla \emph{radice del suono}, ci si pone
trasversalmente, lungo il periodo d'apprendimento di uno studente, a qualsiasi
livello. Non è dunque la quantità di nozioni (o unità) che si collezionano con
la lettura, quanto la qualità dell'esperienza formativa a tracciare il percorso.

\begin{quote}
  Cominciare con uno studio del “tempo” e delle "onde"\ldots È li che comincia
  il moto, quindi il suono. È li che musicista e tecnico cominciano ad
  incontrarsi. Prima di tutte le cognizioni di acustica ed elettroacustica o di
  tecnologia elettronica, è li che si può misurare quello che diventa il "nuovo"
  tecnico-musicale introdotto tramite i mezzi elettronici: com'è stato detto, si
  tratta di "comporre il suono, non comporre col suono".
\end{quote}

La composizione come materia di studio sembra non appartenere più alle classi di
tecnologia. Il mezzo tecnico ha completamente inebriato le menti e distolto
l'attenzione dall'\emph{osservazione mediante composizione} di ciò che percepiamo,
ovvero, ancora: il suono. Il desiderio è di offrire allo studente un percorso
proprio di creatività stimolando la percezione di regole di comprensione del
proprio operato e della relazione di questo con il mondo percepito, per

\begin{quote}
  \ldots andare alla radice del suono. In quest'ambito possono avere più senso
  i vari problemi, come quello di mirare a costruire timbri nuovi\ldots studiare
  per analisi, gli strumenti naturali, in maniera da usare gli strumenti naturali
  come termini di confronto cui l'orecchio è abituato, cioè scritti in codice
  forte.

  Dalla radice del suono, come fatto fisico, si può passare alla radice della
  percezione e della realizzazione sonora.
\end{quote}

Questo ragionamento può essere banale ma necessita di essere messo tra le cose, 
dato lo stato delle scuole di Musica Elettronica in Italia. Si tratta di
imparare a scrivere attraverso la storia della scrittura, dei meccanismi che
hanno resistito alla storia per finire nel linguaggio comune. Nella musica
elettronica, nelle tecnologie musicali, nei libri ad unità, si procede per
atemporalità, per meccanismi e tecnicismi avulsi dall'essere nel tempo delle
cose. Si spiegano le tecniche senza su queste fare quello che semplicemente si
fa in altri campi di studio: scrivere, elaborare creativamente, comporre. Come
se la composizione fosse una questione esclusiva dei compositori, e non uno
strumento di pensiero.

Insegnare a comporre, a scrivere, attraverso il ragionameto, è esattamente una
questione tecnica

\begin{quote}
  faranno bene ad essere quanto più rigorosi teoriocamente e quanto più
  preparati tecnologicamente, in maniera da poter maneggiare tutte le
  apparecchiature possibili\ldots È la via della didattica, quella a cui
  intelligentemente si rifà questo libro di Branchi\ldots

  Esso non è un testo pensato a priori per insegnare le tecniche elettroniche,
  ma è \emph{il risultato} d'una attività didattica già effettuata\ldots Non
  un'aggiunta all'erudizione o un contributo all'indagine teorica, ma, più
  semplicemente e pertinemtemente, uno strumento di lavoro, da utilizzare nella
  pratica.
\end{quote}

Questo lavoro ha la presunzione di non servire a niente se lo scolpo della
lettura è un'apprendimento collezionistico di espedienti e meccanismi.

Questo lavoro vuole e può fornire meccanismi mentali di indagine, sperimentazione,
speculazione, comprensione e composizione che \emph{una scuola} dovrebbe innestare
in alcuni studenti, in alcuni individui. Non in tutti, come nei corsi di lingua
di base, una unità alla volta, ma solo quelli che da questi ragionamenti possano
alzare gli occhi per scoprire di aver cambiato il proprio modo di percepire le
relazioni tra le cose.

% \bigskip
% \noindent\makebox[\linewidth]{\rule{.51\paperwidth}{.5pt}}
% \bigskip

\vfill\null

Questo testo, pur prendendo prezioso esempio dalle pagine di Branchi, vuole fare
diversi passi in molteplici altre direzioni. La distanza temporale dal testo di
Branchi ci permette di guardare a nuovi modi di fare didattica. Il più
importante ed efficae è quello del libero accesso al codice, alla libera
partecipazione al progetto per un'informazione aperta alla discussione.

Questo testo è quindi open source, il progetto risiede all'indirizzo: \\
\url{https://www.gitlab.com/giuseppesilvi/metp}. \\
Atteaverso questo indirizzo si può partecipare alla scrittura, all'integrazione,
alla discussione al fine di rendere il testo vivo, in continua composizione.

%Please note that this project is released with a Contributor Code of Conduct.
%By participating in this project you agree to abide by its terms.

%\end{adjustwidth}

\clearpage
